from .app import app
from flask import render_template
from .models import get_sample, get_book, get_books

@app.route("/")
def home():
    return render_template("home.html",
                            title="Top 10",
                            books = get_sample()
                            )

@app.route("/book")
def books():
    return render_template("books.html",
                            title="Bibliothèque",
                            books = get_books()
                            )



@app.route("/book/<id>")
def book(id):
    book = get_book(id)
    return render_template("book.html",
                            title = book.title,
                            book = book)
