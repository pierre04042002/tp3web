import click
from .app import app, db

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    #création de tables
    db.create_all()

    #Chargement des données
    import yaml
    books = yaml.safe_load(open(filename))

    #import des modèles
    from .models import Author, Book

    #Création des auteurs
    authors = {}
    for b in books :
        a = b["author"]
        if a not in authors:
            o = Author(name=a)
            db.session.add(o)
            authors[a] = o
        db.session.add(o)
    db.session.commit()

    for b in books :
        a = authors[b["author"]]
        o = Book(price     = b["price"],
                 title     = b["title"],
                 url       = b["url"],
                 img       = b["img"],
                 author_id = a.id)
        db.session.add(o)
    db.session.commit()